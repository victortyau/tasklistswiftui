//
//  SectionView.swift
//  TaskListSwiftUI
//
//  Created by Victor Tejada Yau on 5/6/20.
//  Copyright © 2020 Garatge Labs. All rights reserved.
//

import SwiftUI

struct SectionView: View {
    
    @Binding var prioritizedTasks: TaskStore.PrioritizedTasks
    
    var body: some View {
        
        Section(
            header: Text(
                "\(prioritizedTasks.priority.rawValue.capitalized) Priority"
            )
            ) {
                
            ForEach( Array( prioritizedTasks.tasks.enumerated() ), id: \.element.id  ) { index, _ in
                RowView(task: self.$prioritizedTasks.tasks[index])
            }
            .onMove { sourceIndices, destinationIndices in
                self.prioritizedTasks.tasks.move(fromOffsets: sourceIndices, toOffset: destinationIndices)
                
            }
            .onDelete { indexSet in
                self.prioritizedTasks.tasks.remove(atOffsets: indexSet)
            }
        }
        
        
        
    }
}
