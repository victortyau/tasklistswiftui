//
//  ContentView.swift
//  TaskListSwiftUI
//
//  Created by Victor Tejada Yau on 5/6/20.
//  Copyright © 2020 Garatge Labs. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var taskStore: TaskStore
    @State var modalIsPresented = false
    var body: some View {
        
        NavigationView {
            List{
                ForEach( Array( taskStore.prioritizedTasks.enumerated() ),
                         id: \.element.id
                         ) { index, _  in
                    SectionView(prioritizedTasks: self.$taskStore.prioritizedTasks[index])
                }
            }
        .listStyle(GroupedListStyle())
        .navigationBarTitle("Tasks")
        .navigationBarItems(
            leading: EditButton(),
                trailing:
            Button(action: {
                self.modalIsPresented = true
            }){
                Image(systemName: "plus")
            }
            )
        }
        .sheet(isPresented: $modalIsPresented) {
            NewTaskView(taskStore: self.taskStore)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView( taskStore: TaskStore() )
    }
}

