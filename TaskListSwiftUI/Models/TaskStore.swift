//
//  TaskStore.swift
//  TaskListSwiftUI
//
//  Created by Victor Tejada Yau on 5/6/20.
//  Copyright © 2020 Garatge Labs. All rights reserved.
//
import Combine

class TaskStore: ObservableObject {
   @Published var prioritizedTasks = [
        PrioritizedTasks(
            priority: .high,
            names:  [
                  "Fedora Linux",
                  "Fedora Linux",
                  "Fedora Linux",
                  "Fedora Linux",
                  "Fedora Linux",
                  "Fedora Linux",
                  "Fedora Linux",
                  "Fedora Linux",
                  "Fedora Linux",
                  "Fedora Linux"
            ]
        ),
        PrioritizedTasks(
            priority: .medium,
            names:  [
                  "Centos Linux",
                  "Centos Linux",
                  "Centos Linux",
                  "Centos Linux",
                  "Centos Linux"
            ]
        ),
        PrioritizedTasks(
            priority: .low,
            names:  [
                  "Red Hat Linux",
                  "Red Hat Linux",
                  "Red Hat Linux",
                  "Red Hat Linux",
                  "Red Hat Linux"
            ]
        ),
        PrioritizedTasks(
            priority: .no,
            names:  [
                  "Red Hat Enterprise Linux",
                  "Red Hat Enterprise Linux",
                  "Red Hat Enterprise Linux",
                  "Red Hat Enterprise Linux",
                  "Red Hat Enterprise Linux"
            ]
        )
    
    ]
    
    func getIndex(for priority: Task.Priority) -> Int {
        prioritizedTasks.firstIndex { $0.priority == priority  }!
    }
}

extension TaskStore.PrioritizedTasks {
    init(priority: Task.Priority, names: [String]) {
        self.init(
            priority: priority,
            tasks: names.map { Task(name: $0) }
        )
    }
}
