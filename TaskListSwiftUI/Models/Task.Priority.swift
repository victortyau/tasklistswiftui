//
//  Task.Priority.swift
//  TaskListSwiftUI
//
//  Created by Victor Tejada Yau on 5/6/20.
//  Copyright © 2020 Garatge Labs. All rights reserved.

extension Task {
    enum Priority:  String, CaseIterable {
        case no, low, medium, high
    }
}
