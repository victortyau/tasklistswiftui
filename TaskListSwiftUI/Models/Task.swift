//
//  Task.swift
//  TaskListSwiftUI
//
//  Created by Victor Tejada Yau on 5/6/20.
//  Copyright © 2020 Garatge Labs. All rights reserved.
//

import Foundation

struct Task: Identifiable {
    let id = UUID()
    var name: String
    var completed = false
}
